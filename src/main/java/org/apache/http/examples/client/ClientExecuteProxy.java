/*
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

package org.apache.http.examples.client;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * How to send a request via proxy.
 *
 * 在如今的互联网时代，代理服务器已屡见不鲜，遍地可寻，其对互联网用户的重要性自然不言而喻。同样的，代理服务器对于爬虫来说一样重要，
 * 不仅可以通过代理进入某些比较封闭的局域网内抓取数据
 * ，更可以通过切换代理服务器来达到隐藏自身避免被封杀的目的。如果您打算或正在基于HttpClient4.x开发网络爬虫
 * ，且需要通过代理服务器来进行网页抓取，那么本文或许能够帮助您快速将HttpClient4.x的使用代理功能加入到您的爬虫中。
 * 如下代码演示了如何使用HttpClient通过代理服务器发送请求
 * 
 * @since 4.0
 */
public class ClientExecuteProxy {

	public static void main(String[] args) throws Exception {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {
			//目标服务器
			HttpHost target = new HttpHost("www.baidu.com", 80, "https");
			//创建代理服务器
			HttpHost proxy = new HttpHost("119.48.180.72", 8090, "http");

			RequestConfig config = RequestConfig.custom().setProxy(proxy)
					.build();
			HttpGet request = new HttpGet("/");
			request.setConfig(config);

			System.out.println("Executing request " + request.getRequestLine()
					+ " to " + target + " via " + proxy);

			//执行请求
			CloseableHttpResponse response = httpclient
					.execute(target, request);
			try {
				System.out.println("----------------------------------------");
				System.out.println(response.getStatusLine());
				//获得响应实体
				System.out.println(EntityUtils.toString(response.getEntity()));
			} finally {
				response.close();
			}
		} finally {
			httpclient.close();
		}
	}

}
